echo "Tree Structure:"

mkdir dinesh_clidrill-1
cd  dinesh_clidrill-1
mkdir hello
cd hello
mkdir five one
cd five
mkdir six
cd six
touch c.txt
mkdir seven
cd seven
touch error.log
cd ../../..
cd one
touch a.txt b.txt
mkdir two
cd two
touch d.txt
mkdir three
cd three
touch e.txt
mkdir four
cd four
touch access.log
cd ../../../../..
tree
sleep 3

echo "Deleteing .log files:"
find . -name "*.log"|xargs rm
tree
sleep 3

echo "Creating text file:"
echo "Unix is a family of multitasking, multiuser computer operating 
systems that derive from the original AT&T Unix, development 
starting in the 1970s at the Bell Labs research center by Ken 
Thompson, Dennis Ritchie, and others." > ./hello/one/a.txt 
echo "Displaying the text File:"
cat  ./hello/one/a.txt
sleep 3

echo "Deleting folder Five"
rm -r ./hello/five
tree
sleep 3
cd hello

echo "Renaming folder one to uno:"
mv one uno
tree
cd ..
tree
sleep 3

echo "Moving a.txt file into folder-two:"

mv ./hello/uno/a.txt ./hello/uno/two
tree
sleep 3
